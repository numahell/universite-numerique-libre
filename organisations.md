# Organisations d'éducation du numérique

## Bretagne

* [Université populaire du numérique de Damgan](http://adn56.net/)

## Occitanie

* [Combustible](http://combustible.fr/)
* [Toulibre](http://toulibre.org/) (organisent le Capitole du libre, occasionnellement proposent des ateliers)

etc.
