# Ressources

* [Présentation des forums](https://framagit.org/jcfrog/pres-forums) - JC Frog
* [Base de claviers souris](https://framagit.org/jcfrog/bases-clavier-souris) - JC Frog
* [Logiciels libres, et aussi](https://github.com/toulibre/slides-logiciels-libres) - Toulibre
* [Wordpress initiation](https://github.com/toulibre/slides-logiciels-libres) (surtout plan de démo) - Toulibre
* [Retouche et montage photo avec Gimp](https://github.com/toulibre/slides-logiciels-libres)) - Toulibre
* [Multibao](http://www.multibao.org/) plateforme regroupant de nombreuses ressources sur l'animation et la coopération
